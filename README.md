Sample Android Application: A Micropoller
=========================================

This is a simple Android application intended to demonstrate some of the
basic techniques and concepts for putting together an Android application.

To obtain the code:

	$ git clone git@github.com:xebia/micropoll-android.git

To build:

	$ cd micropoll-android
	$ mvn verify

To deploy and run on an attached Android device:

	$ mvn android:deploy android:run
