package com.xebia.nl.micropoll.activities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.common.base.Joiner;
import com.google.common.primitives.Ints;
import com.xebia.nl.micropoll.R;

public class MicroPollActivity extends Activity {
    private static final Logger logger = LoggerFactory.getLogger(MicroPollActivity.class);

    public static final String EXTRA_POLL_RESULTS = "com.xebia.nl.micropoll.extra.POLL_COUNTERS";

    private final int[] counters = new int[3];

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.micropoll);
        if (null != savedInstanceState) {
            final int[] savedCounters = savedInstanceState.getIntArray(EXTRA_POLL_RESULTS);
            if (null != savedCounters && savedCounters.length == counters.length) {
                System.arraycopy(savedCounters, 0, counters, 0, counters.length);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(EXTRA_POLL_RESULTS, counters);
    }

    public void onVoteHigh(View sender) {
        logger.info("Vote: high");
        ++counters[0];
    }

    public void onVoteMedium(View sender) {
        logger.info("Vote: medium");
        ++counters[1];
    }

    public void onVoteLow(View sender) {
        logger.info("Vote: low");
        ++counters[2];
    }

    @Override
    public void onBackPressed() {
        // Instead of just killing this activity, return the current results.
        final Intent pollResults = new Intent();
        pollResults.putExtra(EXTRA_POLL_RESULTS, counters);
        setResult(RESULT_OK, pollResults);
        if (logger.isInfoEnabled()) {
            final String pollDescription = Joiner.on(',').join(Ints.asList(counters));
            logger.info("Returning poll results: {}", pollDescription);
        }
        finish();
    }
}
